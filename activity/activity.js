// 2. Use the project operator to display fruits per country

db.fruits.aggregate([
    { $project: { _id : "$name"}}
])


// 2. Use the count operator to count the total number of fruits on sale.


db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "onSale" }
])


// 3. Use the count operator to count the total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
    { $match: { stock: {$gte:20} } },
    { $count: "stock" }
])

// 4. Use the average operator to get the average price of fruits onSale per supplier.
// $avg

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } }
])


// 5. Use the max operator to get the highest price of a fruit per supplier.
// $max

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", max_price: {$max: "$price"} } }
])


// 6. Use the min operator to get the lowest price of a fruit per supplier.
// $min

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", max_price: {$min: "$price"} } }
])
